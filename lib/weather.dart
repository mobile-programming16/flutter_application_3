import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false,
      home: Scaffold(
          extendBodyBehindAppBar: true,
          body: Stack(
                  children: <Widget>[
                    Container(
                      decoration:BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage("https://t4.ftcdn.net/jpg/04/61/23/23/360_F_461232389_XCYvca9n9P437nm3FrCsEIapG4SrhufP.jpg"),
                            fit: BoxFit.cover,
                          )
                      ) ,
                    ),


                    Container(
                      child: ListView(
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Padding(padding: const EdgeInsets.all(50.0),
                                child: Icon(
                                  Icons.sunny,
                                  size: 200,
                                  color: Colors.white,

                                ),


                              ),

                              _temp(),
                              _location(),
                              _TOday(),
                              _temprow(),
                              _tempcolum()

                            ],
                          ),
                        ],
                      ),
                    )

                    // Column(
                    //
                    // )
                  ],
                ),
              ),
          );

  }
}
_TOday(){
  return Row(
    children: [

      Text("TODAY : 27/28/2022 ",style: TextStyle(fontSize: 20,
          color: Colors.black
      ),
      ),

    ],
  );
}
_location(){
  return Row(
    children: [
      Icon(Icons.location_on,color: Colors.black,)
      ,
      SizedBox(
        width: 10,


      ),
      Text("Bangsaen,Chonburi",style: TextStyle(fontSize: 20,
          color: Colors.black
      ),
      ),

    ],
  );
}


_temp(){
  return Text("-10 ํ",style: TextStyle(fontSize: 80,
      fontWeight: FontWeight.w400,
      color: Colors.black

  ),
    textAlign: TextAlign.center,
  );
}
final tims = ['29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ','29 ํ'] ;

_temprow(){
  return Container(
    height: 100,
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(
            color: Colors.black),
        bottom: BorderSide(color: Colors.black),
      ),

    ),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: tims.length ,
      itemBuilder: (context,index){

        return Container(
          width: 50,
          child: Center(
            child: Text('${tims[index]}',
              style: TextStyle(color: Colors.black),),
          ),
        );
      },
    ),
  );
}
final num = ['tomorrow : sunny','2','3','1','2','3','1','2','3','1','2','3'] ;
_tempcolum(){
  return Container(
    height: 310,
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(
            color: Colors.black),
        bottom: BorderSide(color: Colors.black),
      ),

    ),
    child: ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: num.length ,
      itemBuilder: (context,index){

        return Container(
          height: 50,
          child: Center(
            child: Text('${num[index]}',
              style: TextStyle(color: Colors.black),),
          ),
        );
      },
    ),
  );

}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0,
    centerTitle: true,
    title: Text(
      "weather",

    ),
    leading: Icon(
      Icons.add,
      color: Colors.black,
    ),
    actions: [
      Icon(
        Icons.more_vert,
        color: Colors.black,
      )
    ],
  );
}

